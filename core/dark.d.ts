declare module '@uiw/react-json-view/dark' {
  export const darkTheme: {
    '--w-rjv-font-family': string;
    '--w-rjv-color': string;
    '--w-rjv-background-color': string;
    '--w-rjv-border-left': string;
    '--w-rjv-arrow-color': string;
    '--w-rjv-info-color': string;
    '--w-rjv-type-string-color': string;
    '--w-rjv-type-int-color': string;
    '--w-rjv-type-float-color': string;
    '--w-rjv-type-bigint-color': string;
    '--w-rjv-type-boolean-color': string;
    '--w-rjv-type-date-color': string;
    '--w-rjv-type-null-color': string;
    '--w-rjv-type-nan-color': string;
    '--w-rjv-type-undefined-color': string;
  };
}
