export const lightTheme = {
  '--w-rjv-font-family': 'monospace',
  '--w-rjv-color': '#002b36',
  '--w-rjv-background-color': '#ffffff',
  '--w-rjv-line-color': '#ebebeb',
  '--w-rjv-arrow-color': 'var(--w-rjv-color)',
  '--w-rjv-info-color': '#0000004d',
  '--w-rjv-copied-color': '#002b36',
  '--w-rjv-copied-success-color': '#28a745',

  '--w-rjv-curlybraces-color': '#236a7c',
  '--w-rjv-brackets-color': '#236a7c',

  '--w-rjv-type-string-color': '#cb4b16',
  '--w-rjv-type-int-color': '#268bd2',
  '--w-rjv-type-float-color': '#859900',
  '--w-rjv-type-bigint-color': '#268bd2',
  '--w-rjv-type-boolean-color': '#2aa198',
  '--w-rjv-type-date-color': '#586e75',
  '--w-rjv-type-null-color': '#d33682',
  '--w-rjv-type-nan-color': '#859900',
  '--w-rjv-type-undefined-color': '#586e75',
};
